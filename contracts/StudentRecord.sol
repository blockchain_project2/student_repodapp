// SPDX-License-Identifier: MIT
pragma experimental ABIEncoderV2;
pragma solidity >=0.4.22 <0.9.0;

contract StudentRecord {
    uint public studentsCount = 0;

     // Model a Student
    struct Student {
        uint _id; //unit it represents a unique value to identify the students
        uint sid;
        string name;
        bool graduated;
        uint weight;
        uint height;

    }
    mapping(uint => Student) public students;//mapping is similar to store data in terms of solidity.
//this constructor will be called whenever the contract is published to blockchain
    constructor() public{ 
        // addStudent( 12210003, "Choki Lhamo");
    }
    // Events
    //when significan event occurs in smart contract this event will help to convey other smart contracts.
    // we create this event to broadcast each time a student object is generated(new student)
    event addStudentEvent (
        uint _id,
        uint indexed sid,
        string name,
        bool graduated,
        uint weight,
        uint height
    );

    //when the contract is being deployed, it will call addstudent function which emits the addstudent event.

    //Create and add student to storage
    //addstudent function takes 1 argument of string type _name. memory keyword means it will persist in memory rather than storage
    //return is set as pulic so that this contract can be  called outside the smart contract
    function addStudent(uint _studentNumber, string memory _name, uint _weight, uint _height) 
        public returns (Student memory){
        studentsCount++; //inside the function we are creating id for student by incrementing the count of student
        students[studentsCount] = Student(studentsCount,_studentNumber, _name, false, _weight, _height);//we are storing new student object into blockchain by adding it inot mapping(students[studentscount])
        //trigger create event
        emit addStudentEvent(studentsCount,_studentNumber, _name, false, _weight, _height);//we must call the event at a location where we want to broadcast it
        //right after initialising the student object

        return students[studentsCount];
    }
    event markGraduatedEvent (
        uint indexed sid
    );
    //Change graduation status of student
    //we need to pass the id of student so that specific student object is retrieved
    function markGraduated(uint _id) 
        public returns (Student memory) {
        students[_id].graduated = true;
        // trigger create event
        emit markGraduatedEvent(_id);
        return students[_id];
    }
    //Fetch student info from storage
    function findStudent(uint _id) public view returns (Student memory) {
        return students[_id];
    }
     event markunGraduatedEvent (
        uint indexed sid
    );
    //Change graduation status of student
    //we need to pass the id of student so that specific student object is retrieved
    function markunGraduated(uint _id) 
        public returns (Student memory) {
        students[_id].graduated = true;
        // trigger create event
        emit markunGraduatedEvent(_id);
        return students[_id];
    }
   
    
    

}

//we must test the smart contrace because once it is begin deployed we cannot alter it and we might have to create  new contract
//contract deployment will generate transaction and writes it into blockchain.
//if there is any erro in this contract, it will be waste of ether 


//inorder to test our smart contract we creaete student.test.js



