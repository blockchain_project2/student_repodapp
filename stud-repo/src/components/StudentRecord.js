
import React, { Component } from 'react'
 
class StudentRecord extends Component {
  render() {
    return (
      <form className="p-5 border" onSubmit={(event) => {event.preventDefault()
            this.props.addStudent(this.sid.value, this.student.value, this.weight.value, this.height.value)
      }}>
        <h2>Add Student</h2>
        <input id="newCID" type="text"
          ref={(input) => { this.sid = input; }}
          className="form-control m-1"
          placeholder="SID"
          required
        />
        <input id="newStudent" type="text"
          ref={(input) => { this.student = input; }}
          className="form-control m-1"
          placeholder="Student Name"
          required
        />
          <input id="newWeight" type="number"
          ref={(input) => { this.weight = input; }}
          className="form-control m-1"
          placeholder="weight"
          required
        />
              <input id="newHeight" type="number"
          ref={(input) => { this.height = input; }}
          className="form-control m-1"
          placeholder="height"
          required
        />
        <input className="form-control btn-primary" type="submit" hidden="" />
      </form>
    );
  }
}
export default StudentRecord;




