// import { assert } from "node:console"
// import { it } from "node:test"

//Import the StudentList smart contract
const StudentRecord = artifacts.require('StudentRecord')//artifacts in this is dist folder.
 
//Use the contract  to write all tests
//variable: account => all accounts in blockchain
//arrow function is callback.
contract('StudentRecord', (accounts) => { //this callback function has variable accounts which represents all the accounts in ganache
    //Make sure contract is deployed and before
    //we retrieve the studentrecord object for testing
    beforeEach(async () => {
        this.studentRecord = await StudentRecord.deployed()
    })
 
    //Testing the deployed student contract
    it('Deployed successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.studentRecord.address
        //Test for valid address
        isValidAddress(address)
    })
    //Testing the content in the contract
    it('Added the students successfully', async () => {
        return StudentRecord.deployed().then((instance) => {
        studentrecordInstance = instance;
        studentNo = 12210009;
        return studentrecordInstance.addStudent(studentNo, "Jigme Namgyel", 200, 70);
        }).then((transaction) => {
        isValidAddress(transaction.tx)
        isValidAddress(transaction.receipt.blockHash);
        return studentrecordInstance.studentsCount()
        }).then((count) => {
        assert.equal(count, 1)
        return studentrecordInstance.students(1);
        }).then((student) => {
        assert.equal(Number(student.sid), Number(studentNo))
        })
    })
     // find the student
     it('Successful search of student', async () => {
        return StudentRecord.deployed().then(async (instance) => {
            s = instance;
            studentid = 2;
            return s.addStudent(studentid++, "Pema Yangzom",300, 71).then(async (tx) => {
            return s.addStudent(studentid++, "Rigden Yoesel", 400, 75).then(async (tx) => {
            return s.addStudent(studentid++, "Sonam Tshering", 325, 72).then(async (tx) => {
            return s.addStudent(studentid++, "Tshering Dorji",355, 72).then(async (tx) => {
            return s.addStudent(studentid++, "Bijay Kumar Rai",233, 50).then(async (tx) => {
            return s.addStudent(studentid++, "Chencho Dema", 200, 68).then(async (tx) => {
            return s.studentsCount().then(async (count) => {
            assert.equal(count, 7)
     
            return s.findStudent(5).then(async (student) => {
                assert.equal(student.name, "Tshering Dorji")
            })
            })
            })
            })
            })
            })
            })
            })
        })
    })
    // Mark graduation of student
    it('Successfully marked graduate students', async () => {
        return StudentRecord.deployed().then(async (instance) => {
            s = instance;
            return s.findStudent(1).then(async (ostudent) => {
            assert.equal(ostudent.name, "Jigme Namgyel")
            assert.equal(ostudent.graduated, false)
            return s.markGraduated(1).then(async (transaction) => {
                isValidAddress(transaction.tx)
                isValidAddress(transaction.receipt.blockHash);
                return s.findStudent(1).then(async (nstudent) => {
                assert.equal(nstudent.name, "Jigme Namgyel")
                assert.equal(nstudent.graduated, true)
                return
                })
            })
            })
        })
    })


   

})
//This function check if the address is valid
//checking if the contract is deployed successfully in blockchain by inspecting its address
function isValidAddress(address){
  assert.notEqual(address, 0x0)
  assert.notEqual(address, '')
  assert.notEqual(address, null)
  assert.notEqual(address, undefined)
}

