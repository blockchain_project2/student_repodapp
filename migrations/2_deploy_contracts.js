var StudentRecord = artifacts.require("./StudentRecord.sol");//easiest way to include modules that are in different files
module.exports = function(deployer) {
  deployer.deploy(StudentRecord);
};
//module.exports is the instructions of nodejs that tells us which bits of codes (functions,string,objects) to export from a given file, so that
//other files are allowed to access the exported code 